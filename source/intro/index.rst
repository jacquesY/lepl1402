	.. _intro:


************
Organization
************


Pedagogy
=======================================



We will use *Java* and more specifically Java8_.
The recommended IDE is IntelliJ_.
We will use Junit4_ to perform unit tests.
It is highly recommended to become familiar with these tools to get ready for the exam.

.. _Java8: https://docs.oracle.com/javase/8/docs/api.
.. _IntelliJ: https://www.jetbrains.com/idea/
.. _Inginious: https://inginious.info.ucl.ac.be
.. _JUnit4: https://junit.org/junit4/.

We will essentially introduce the concepts through lectures in auditorium.
The exercices should be achieved individually on Inginious.


Link with English course
=======================================

The `Moodle <https://moodleucl.uclouvain.be/course/view.php?id=12884>`_. of this course has a MoodleOverflow (kind of StackExchange).
The English teachers ask that every student actively 

1. propose typical exam question (on the already covered material)
2. propose answers to others-questions and or direction of answers and rate/improve others-answers 

The exam question proposal has to be completed before english course.
This activity will be evaluated by the english teacher (writing skills).

A good question is a question that may be asked at the exam.
Here are some guidelines for proposing a good question:

* The answer should not be too straightforward.
* The question should not be ambiguous (don't hesitate to illustrate the question with an example of input/output).
* The question should cover only material seen during the course but can possibly overlap with several modules.
* For programming questions, there should be an elegant answers in less than 20 lines of code.

Motivation: We will select "good" questions at the exam.


Course Open-Source
=======================================

This website and the teaching material it contains is open-source `bitbucket <https://bitbucket.org/pschaus/lepl1402/src>`_.
We welcome pull requests to fix errors or proposing new exercises.
The license is Creative Commons Attribution-ShareAlike 4.0 International License:

.. image:: https://i.creativecommons.org/l/by-sa/4.0/88x31.png
    :alt: CC-BY-SA


Agenda
=======================================

The course is organized in 6 modules of 2 weeks each.

* Thursday w1: course by Prof. P. Schaus (modules 1-3) or Prof. Ramin Sadre (modules 4-6)
* w1: preparation of exercises (TA in lab room on Wednessday 8h30 and 14h00)
* Monday w2: course by Prof. P. Schaus (modules 1-3) or Prof. Ramin Sadre (modules 4-6)
* w2: preparation of exercises (TA in lab room on Wednessday 8h30 and 14h00)
* Thursday w3: small restructuration by TA [+ start next module]


Evaluation
=======================================

Exam on Inginious + One mid-term quizz during smart-week on Inginious.

The mid-term quizz is for 2 points in the final grade only if it improves the grade.
The active participation (proposing and commenting questions) is for 1 point.
The exam is thus evaluated on 17 (or 19 if the mid-term quizz was less good than the exam).
The same scoring is used in August.

Example1: participation: 1/1, mid-term: 15/20, examen 10/20, total: maximum(10/20*19+1,15/20*2+10/20*17+1)=11.

Example2: participation: 1/1, evaluation mid-term 6: 8/20, examen 12/20, total: maximum(12/20*19+1,8/20*2+12/20*17+1)=12.4


Contact et communication
=======================================


Important communications using moodle.
Also check this page on a regular bases.
For important matters:
`Pierre Schaus <pierre.schaus@uclouvain.be>`_ and
`Ramin Sadre <ramin.sadre@uclouvain.be>`_.