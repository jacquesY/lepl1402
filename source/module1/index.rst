.. _module1:


************************************************************************************************************************
Module 1 | Java Introduction, Static and main methods, Exceptions, Arrays, Dev Tools, Pre-post conditions
************************************************************************************************************************

Objective
=========

By the end of this module, students will be able to:

* Write simple Java programs involving objects, primitive types and arrays
* Understand the difference between static and non-static fields and methods
* Understand object references the consequence of the by value parameter passing
* Be able to program with exceptions
* Be able to document correctly Java programs
* Use the IntelliJ IDE

Resources
=======================================


Slides (keynote)

* `Lecture1 1 <https://www.icloud.com/keynote/0ZD6BWx-nk97atygiLTGXU4Ag#cours1a-java-intro>`_ 
* `Lecture1 2 <https://www.icloud.com/keynote/0LM5W3LX2L4MTr1cFacPZRQPQ#cours1b-java-intro>`_ 

* `W3schools Tutorial on Java <https://www.w3schools.com/java>`_ 

Exercises: week 1
=======================================

.. note::
   Due to Thursday S2.

1. `Intro to Java <https://inginious.info.ucl.ac.be/course/LEPL1402/Introduction>`_
2. `Exceptions <https://inginious.info.ucl.ac.be/course/LEPL1402/LearnException>`_ 
3. `Write code that generates exceptions <https://inginious.info.ucl.ac.be/course/LEPL1402/MakeMistakeToUnderstandThem>`_ 
4. `1D and 2D arrays <https://inginious.info.ucl.ac.be/course/LEPL1402/Array2D>`_ 


Exercise 1.1.1
""""""""""""""

* Write a java program "Calculator" that takes a series of int argument from the command line, make the summation and prints the result. Hint `parseInt <https://docs.oracle.com/javase/8/docs/api/java/lang/Integer.html#parseInt-java.lang.String->`_  method may be useful.
* Compile your program using javac (verify that the file ``Calculator.class`` has been generated)
* Visualize the bytecode using javap -v
* Execute your program from the command line and verifies that it works: ``java Calculator 10 2 13`` should output 25
* Now edit your program in IntelliJ and pass the arguments from IntelliJ


Exercises: week 2
=======================================

.. note::
   Due to Thusday S3.

1. `Common errors <https://inginious.info.ucl.ac.be/course/LEPL1402/CommonErrors>`_ 
2. `Value vs References <https://inginious.info.ucl.ac.be/course/LEPL1402/ValueOrReference>`_ 
3. `String <https://inginious.info.ucl.ac.be/course/LEPL1402/StringUtils>`_ 
4. `ASCIIDecoder <https://inginious.info.ucl.ac.be/course/LEPL1402/task/ASCIIDecoder>`_ 
5. `Casting <https://inginious.info.ucl.ac.be/course/LEPL1402/Casting>`_ 
6. `Access Modifiers <https://inginious.info.ucl.ac.be/course/LEPL1402/AccessModifiers>`_ 
7. `Sorting with Comparator and Collections <https://inginious.info.ucl.ac.be/course/LEPL1402/ComparatorAndCollections>`_ 
8. `Sorting with Comparator and Collections <https://inginious.info.ucl.ac.be/course/LEPL1402/ComparatorAndCollections>`_ 
9. `Sieve of Eratostheme MCQ <https://inginious.info.ucl.ac.be/course/LEPL1402/SieveOfEratosthenesMCQ>`_ 
10. `Sieve of Eratostheme Implem <https://inginious.info.ucl.ac.be/course/LEPL1402/SieveOfEratosthenesImplementation>`_ 


