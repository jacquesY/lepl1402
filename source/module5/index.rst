.. _module5:


*************************************************************************************************
Module 5 | Functional Programming and Lambda
*************************************************************************************************

Objective
=========

By the end of this module, students will be able to:

* objective 1
* objective 2

Resourses
=======================================


Slides (keynote)

* `Link1TODO <https://www.icloud.com/keynote/0jTHGv9VcBJNqr701X0LiSSeQ#part1-intro>`_ 

Exercises: week 1
=======================================

.. note::
   Exercises due to Monday S10.

Exercise 5.1.1
""""""""""""""

blabla

Exercise 5.1.2
""""""""""""""

blabla



Inginious Exercises
==========================================

.. note::
   Due to Monday S11.

1. `TODOExercise1 <https://inginious.info.ucl.ac.be/course/LSINF1121-2016/m1stacktests>`_
2. `TODOExercise2 <https://inginious.info.ucl.ac.be/course/LSINF1121-2016/m1stack>`_ 

Exercises: week 2
=======================================

.. note::
   Due to Monday S11.

Exercise 5.2.1
""""""""""""""

.. code-block:: java


   import java.util.ConcurrentModificationException;
   import java.util.Iterator;
   import java.util.NoSuchElementException;

   public class CircularLinkedList<Item> implements Iterable<Item> {
    private long nOp = 0; // count the number of operations
    private int n;          // size of the stack
    private Node  last;   // trailer of the list

    // helper linked list class
    private class Node {
        private Item item;
        private Node next;
    }

    public CircularLinkedList() {
        last = new Node(); // dummy node
        last.next = last;
        n = 1;
    }

    public boolean isEmpty() { return n == 1; }

    public int size() { return n-1; }

    private long nOp() { return nOp; }

    /**
     * Append an item at the end of the list
     * @param item the item to append
     */
    public void enqueue(Item item) {
        // TODO STUDENT: Implement add method
    }

    /**
     * Removes the element at the specified position in this list.
     * Shifts any subsequent elements to the left (subtracts one from their indices).
     * Returns the element that was removed from the list.
     */
    public Item remove(int index) {
        // TODO STUDENT: Implement remove method
    }

    /**
     * Returns an iterator that iterates through the items in FIFO order.
     * @return an iterator that iterates through the items in FIFO order.
     */
    public Iterator<Item> iterator() {
        return new ListIterator();
    }

    /**
     * Implementation of an iterator that iterates through the items in FIFO order.
     *
     */
    private class ListIterator implements Iterator<Item> {
        // TODO STUDENT: Implement the ListIterator
    }

   }

Exercise 1.2.2
""""""""""""""
Blabla

.. code-block:: java


    String in = "4 20 + 3 5 1 * * +";
    StringTokenizer tokenizer = new StringTokenizer(in);
    while (tokenizer.hasMoreTokens()) {
         String element = tokenizer.nextToken();
    }


